<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CallesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/ListarCalles', [CallesController::class, 'ListarCalles']);
Route::get('/ListarRegiones', [CallesController::class, 'ListarRegiones']);
Route::post('/AgregarNuevaCalle', [CallesController::class, 'AgregarNuevaCalle']);
Route::post('/MostrarCalle', [CallesController::class, 'MostrarCalle']);
Route::put('/ModificarCalle', [CallesController::class, 'ModificarCalle']);