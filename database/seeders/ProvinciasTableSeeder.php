<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class ProvinciasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Provincias')->insert([
            [
                "nombre_provincia" => 'Provincia 1',
                'id_region' => 1
            ],
            [
                "nombre_provincia" => 'Provincia 2',
                'id_region' => 1
            ],
            [
                "nombre_provincia" => 'Provincia 3',
                'id_region' => 1
            ],
            [
                "nombre_provincia" => 'Provincia 4',
                'id_region' => 2
            ],
            [
                "nombre_provincia" => 'Provincia 5',
                'id_region' => 2
            ],
            [
                "nombre_provincia" => 'Provincia 6',
                'id_region' => 2
            ],
            [
                "nombre_provincia" => 'Provincia 7',
                'id_region' => 3
            ],
            [
                "nombre_provincia" => 'Provincia 8',
                'id_region' => 3
            ],
            [
                "nombre_provincia" => 'Provincia 9',
                'id_region' => 3
            ],
            [
                "nombre_provincia" => 'Provincia 10',
                'id_region' => 4
            ],
            [
                "nombre_provincia" => 'Provincia 11',
                'id_region' => 4
            ],
            [
                "nombre_provincia" => 'Provincia 12',
                'id_region' => 4
            ],
            [
                "nombre_provincia" => 'Provincia 13',
                'id_region' => 5
            ],
            [
                "nombre_provincia" => 'Provincia 14',
                'id_region' => 5
            ],
            [
                "nombre_provincia" => 'Provincia 15',
                'id_region' => 5
            ],
        ]);
    }
}
