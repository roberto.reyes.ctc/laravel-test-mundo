<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CiudadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ciudades')->insert([
            [
                "nombre_ciudad" => "Ciudad 1",
                "id_provincia" => 1
            ],
            [
                "nombre_ciudad" => "Ciudad 2",
                "id_provincia" => 1
            ],
            [
                "nombre_ciudad" => "Ciudad 3",
                "id_provincia" => 2
            ],
            [
                "nombre_ciudad" => "Ciudad 4",
                "id_provincia" => 2
            ],
            [
                "nombre_ciudad" => "Ciudad 5",
                "id_provincia" => 3
            ],
            [
                "nombre_ciudad" => "Ciudad 6",
                "id_provincia" => 3
            ],
            [
                "nombre_ciudad" => "Ciudad 7",
                "id_provincia" => 4
            ],
            [
                "nombre_ciudad" => "Ciudad 8",
                "id_provincia" => 4
            ],
            [
                "nombre_ciudad" => "Ciudad 9",
                "id_provincia" => 5
            ],
            [
                "nombre_ciudad" => "Ciudad 10",
                "id_provincia" => 5
            ],
            [
                "nombre_ciudad" => "Ciudad 11",
                "id_provincia" => 6
            ],
            [
                "nombre_ciudad" => "Ciudad 12",
                "id_provincia" => 6
            ],
            [
                "nombre_ciudad" => "Ciudad 13",
                "id_provincia" => 7
            ],
            [
                "nombre_ciudad" => "Ciudad 14",
                "id_provincia" => 7
            ],
            [
                "nombre_ciudad" => "Ciudad 15",
                "id_provincia" => 8
            ],
            [
                "nombre_ciudad" => "Ciudad 16",
                "id_provincia" => 8
            ],
            [
                "nombre_ciudad" => "Ciudad 17",
                "id_provincia" => 9
            ],
            [
                "nombre_ciudad" => "Ciudad 18",
                "id_provincia" => 9
            ],
            [
                "nombre_ciudad" => "Ciudad 19",
                "id_provincia" => 10
            ],
            [
                "nombre_ciudad" => "Ciudad 20",
                "id_provincia" => 10
            ],
            [
                "nombre_ciudad" => "Ciudad 21",
                "id_provincia" => 11
            ],
            [
                "nombre_ciudad" => "Ciudad 22",
                "id_provincia" => 11
            ],
            [
                "nombre_ciudad" => "Ciudad 23",
                "id_provincia" => 12
            ],
            [
                "nombre_ciudad" => "Ciudad 24",
                "id_provincia" => 12
            ],
            [
                "nombre_ciudad" => "Ciudad 25",
                "id_provincia" => 13
            ],
            [
                "nombre_ciudad" => "Ciudad 26",
                "id_provincia" => 13
            ],
            [
                "nombre_ciudad" => "Ciudad 27",
                "id_provincia" => 14
            ],
            [
                "nombre_ciudad" => "Ciudad 28",
                "id_provincia" => 14
            ],
            [
                "nombre_ciudad" => "Ciudad 29",
                "id_provincia" => 15
            ],
            [
                "nombre_ciudad" => "Ciudad 30",
                "id_provincia" => 15
            ],
        ]);
    }
}
