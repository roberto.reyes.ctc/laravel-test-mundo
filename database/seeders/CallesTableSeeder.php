<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CallesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Calles')->insert([
            [
                "nombre_calle" => "Calle 1",
                "id_ciudad" => 1
            ],
            [
                "nombre_calle" => "Calle 2",
                "id_ciudad" => 2
            ],
            [
                "nombre_calle" => "Calle 3",
                "id_ciudad" => 3
            ],
            [
                "nombre_calle" => "Calle 4",
                "id_ciudad" => 4
            ],
            [
                "nombre_calle" => "Calle 5",
                "id_ciudad" => 5
            ]
        ]);
    }
}
