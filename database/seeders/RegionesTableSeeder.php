<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class RegionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Regiones')->insert([
            [
                'nombre_region' => 'Región 1'
            ],
            [
                'nombre_region' => 'Región 2'
            ],
            [
                'nombre_region' => 'Región 3'
            ],
            [
                'nombre_region' => 'Región 4'
            ],
            [
                'nombre_region' => 'Región 5'
            ],
        ]);
    }
}
