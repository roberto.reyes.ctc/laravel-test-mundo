<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProvinciaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre_provincia' => $faker->text($maxNbChars = 200),
            'id_region' => $faker->randomDigitNotNull,
        ];
    }
}
