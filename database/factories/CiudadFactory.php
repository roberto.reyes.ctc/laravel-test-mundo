<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CiudadFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre_ciudad' => $faker->text($maxNbChars = 200),
            'id_provincia' => $faker->randomDigitNotNull,
        ];
    }
}
