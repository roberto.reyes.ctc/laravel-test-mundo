<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Provincias extends Model
{
    use HasFactory;

    public function ciudades() {
        return $this->hasMany('App\Models\Ciudades', 'id_provincia');
    }

    public function regiones () {
        return $this->belongsTo('App\Models\Regiones');
    }
}
