<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Regiones extends Model
{
    use HasFactory;

    public function provincias() {
        return $this->hasMany('App\Models\Provincias', 'id_region');
    }
}
