<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ciudades extends Model
{
    use HasFactory;

    public function provincias() {
        return $this->belongsTo('App\Models\Provincias', 'id_provincia');
    }

    public function calles() {
        return $this->hasMany('App\Models\Calles', 'id_ciudad');
    }
}
