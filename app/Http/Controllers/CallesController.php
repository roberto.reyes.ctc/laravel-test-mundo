<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ciudades;
use App\Models\Provincias;
use App\Models\Regiones;
use App\Models\Calles;

class CallesController extends Controller
{
    public function ListarRegiones() {
        $regiones = Regiones::all();
        $auxRegiones = $regiones;

        foreach($auxRegiones as $keyR => $region) {
            $provincias = Provincias::where('id_region', $region->id_region)
                ->get();
            $auxProvincias = $provincias;
            
            foreach($auxProvincias as $keyP => $provincia) {
                $ciudades = Ciudades::where('id_provincia', $provincia->id_provincia)
                    ->get();
                
                $provincias[$keyP]->ciudades = $ciudades;
            }

            $regiones[$keyR]->provincias = $provincias;
        }

        return response()->json($regiones);
    }

    public function AgregarNuevaCalle(Request $request) {
        $request = json_decode($request->getContent(), true);
        $response = [];

        if(!isset($request["calle"]) || !isset($request["ciudad"]))
            return response()->json("no está");

        $calle = new Calles();
        $calle->nombre_calle = $request["calle"];
        $calle->id_ciudad = $request["ciudad"];
        $calle->save();

        return response()->json('Done');
        
                
    }

    public function ListarCalles() {
        $calles = Calles::all("id_calle", "nombre_calle", "id_ciudad");
        $aux = $calles;

        foreach($aux as $key => $calle) {
            $ciudad = Ciudades::select("nombre_ciudad", "id_provincia")
                ->where('id_ciudad', $calle->id_ciudad)
                ->first();
            
            $provincia = Provincias::select("nombre_provincia", "id_region")
                ->where("id_provincia", $ciudad->id_provincia)
                ->first();

            $region = Regiones::select("nombre_region")
                ->where("id_region", $provincia->id_region)
                ->first();

            $calles[$key]->nombre_ciudad = $ciudad->nombre_ciudad;
            $calles[$key]->id_provincia = $ciudad->id_provincia;
            $calles[$key]->nombre_provincia = $provincia->nombre_provincia;
            $calles[$key]->id_region = $provincia->id_region;
            $calles[$key]->nombre_region = $region->nombre_region;
        }

        return response()->json($calles);
    }

    public function MostrarCalle(Request $request) {
        $request = json_decode($request->getContent(), true);

        if(!isset($request["calle"]))
            return response()->json(["Estado" => "Fail"]);

        $calle = Calles::select("id_calle", "nombre_calle", "id_ciudad")
            ->where("id_calle", $request["calle"])
            ->first();

        if($calle === null)
            return response()->json(["Estado" => "Fail"]);
            
        $ciudad = Ciudades::select("nombre_ciudad", "id_provincia")
            ->where("id_ciudad", $calle->id_ciudad)
            ->first();
        $provincia = Provincias::select("nombre_provincia", "id_region")
            ->where("id_provincia", $ciudad->id_provincia)
            ->first();
        $region = Regiones::select("nombre_region")
            ->where("id_region", $provincia->id_region)
            ->first();

        $response = [
            "estado" => "Done",
            "data" => [
                "id_calle" => $calle->id_calle,
                "nombre_calle" => $calle->nombre_calle,
                "id_ciudad" => $calle->id_ciudad,
                "nombre_ciudad" => $ciudad->nombre_ciudad,
                "id_provincia" => $ciudad->id_provincia,
                "nombre_provincia" => $provincia->nombre_provincia,
                "id_region" => $provincia->id_region,
                "nombre_region" => $region->nombre_region
            ]
        ];

        return response()->json($response);
    }

    public function ModificarCalle(Request $request) {
        $request = json_decode($request->getContent(), true);

        if(!isset($request["calle"]) || !isset($request["id_ciudad"]) || !isset($request["id_calle"]))
            return response()->json(["Estado" => "Fail"]);

        Calles::where("id_calle", $request["id_calle"])
            ->update(["nombre_calle" => $request["calle"], "id_ciudad" => $request["id_ciudad"]]);

        return response()->json("Done");
    }
}
