# Laravel Test Mundo

Este proyecto está pensado para ser utilizado junto a [react-test-mundo](https://gitlab.com/roberto.reyes.ctc/react-test-mundo).

## Preparando el ambiente

Antes de pasar a la implementación, es necesario tener los siguientes sistemas previamente instalados.

- Composer versión 2.0.8
- Xampp versión 8.0.0
- MySQL Workbench

## Pasos para implementación

1. Ejecuta Xampp e inicia el módulo de MySQL.
2. Crear un nuevo esquema en Workbench con el nombre de **db_streets_manteiner**.
3. Cambiar el nombre del arcivo **.env.example** a **.env**.
4. Editar este archivo, cambiando el valor de la variable **DB_DATABASE** a **db_streets_manteiner**.
5. Ejecutar los siguientes comandos en la raíz del proyecto:
    ```
    composer install
    php artisan migrate
    php artisan db:seed

    ```
6. Ejecutar el proyecto con el comando `php artisan serve`.


